﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace _01
{
	class Program
	{
		/*
		 * Time Taken: 00:07:11
		 */
		static void Main(string[] args)
		{
			var fileContent = File.ReadAllLines(@".\input1.txt");

			var elvesCalories = new List<int>();
			var currentCalories = 0;
			
			for (var i = 0; i < fileContent.Length; i++)
			{
				var line = fileContent[i];
				
				if (string.IsNullOrWhiteSpace(line))
				{
					elvesCalories.Add(currentCalories);
					currentCalories = 0;
				}
				else
				{
					currentCalories += int.Parse(line);
				}
			}

			var maxCalories = elvesCalories.Max();
			Console.WriteLine($"Elve with max have: {maxCalories}");

			var maxThree = elvesCalories.OrderByDescending(v => v).Take(3).Sum();
			Console.WriteLine($"Top 3 Elves have: {maxThree}");
		}
	}
}
