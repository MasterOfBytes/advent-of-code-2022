﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace _06
{
	class Program
	{

		static void Main(string[] args)
		{
			var fileContent = File.ReadAllLines(@".\input1.txt");

			Console.WriteLine($"01: {M01(fileContent[0], 4)}");
			Console.WriteLine($"02: {M01(fileContent[0], 14)}");

		}

		static int M01(string line, int numberOfDiscinctChars)
		{
			Console.WriteLine(line);
			for (var i = 0; i < line.Length; i++)
			{
				var sub = line.Substring(i, numberOfDiscinctChars);
				if (sub.Distinct().Count() == numberOfDiscinctChars)
				{
					return i + numberOfDiscinctChars;
				}
			}
			throw new Exception("Nope!");
		}
	}
}
