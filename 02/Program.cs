﻿using System;
using System.IO;
using System.Linq;

namespace _02
{
	class Program
	{
		enum itemsopponent
		{
			Rock = 'A',
			Paper = 'B',
			Scissors = 'C',
		}

		enum itemsme
		{
			Rock = 'X',
			Paper = 'Y',
			Scissors = 'Z',
		}

		enum winorloose
		{
			Loose = 'X',
			Draw = 'Y',
			Win = 'Z',
		}
		static void Main(string[] args)
		{
			p_01();
			p_02();
		}

		private static void p_01()
		{
			var fileContent = File.ReadAllLines(@".\input1.txt");

			var score = 0;
			for (var i = 0; i < fileContent.Length; i++)
			{
				var line = fileContent[i];
				var lineSplit = line.Split(' ');

				var opponent = (itemsopponent)lineSplit[0][0];
				var me = (itemsme)lineSplit[1][0];

				switch (me)
				{
					case itemsme.Rock:
						score += 1;
						break;
					case itemsme.Paper:
						score += 2;
						break;
					case itemsme.Scissors:
						score += 3;
						break;
				}

				if ((me == itemsme.Rock && opponent == itemsopponent.Scissors)
					|| (me == itemsme.Scissors && opponent == itemsopponent.Paper)
					|| (me == itemsme.Paper && opponent == itemsopponent.Rock)
					)
				{
					// i won
					score += 6;
				}
				else if ((opponent == itemsopponent.Rock && me == itemsme.Scissors)
					|| (opponent == itemsopponent.Scissors && me == itemsme.Paper)
					|| (opponent == itemsopponent.Paper && me == itemsme.Rock)
					)
				{
					// opponent won
					score += 0;
				}
				else
				{
					// draw
					score += 3;
				}
			}
			Console.WriteLine($"01: {score}");
		}

		private static void p_02()
		{
			var fileContent = File.ReadAllLines(@".\input1.txt");

			var score = 0;
			for (var i = 0; i < fileContent.Length; i++)
			{
				var line = fileContent[i];
				var lineSplit = line.Split(' ');

				var opponent = (itemsopponent)lineSplit[0][0];
				var winorloose = (winorloose)lineSplit[1][0];
				var me = getme(opponent, winorloose);

				switch (me)
				{
					case itemsme.Rock:
						score += 1;
						break;
					case itemsme.Paper:
						score += 2;
						break;
					case itemsme.Scissors:
						score += 3;
						break;
				}

				if ((me == itemsme.Rock && opponent == itemsopponent.Scissors)
					|| (me == itemsme.Scissors && opponent == itemsopponent.Paper)
					|| (me == itemsme.Paper && opponent == itemsopponent.Rock)
					)
				{
					// i won
					score += 6;
				}
				else if ((opponent == itemsopponent.Rock && me == itemsme.Scissors)
					|| (opponent == itemsopponent.Scissors && me == itemsme.Paper)
					|| (opponent == itemsopponent.Paper && me == itemsme.Rock)
					)
				{
					// opponent won
					score += 0;
				}
				else
				{
					// draw
					score += 3;
				}
			}
			Console.WriteLine($"02: {score}");


			itemsme getme(itemsopponent opp, winorloose wl)
			{
				switch (wl)
				{
					case winorloose.Win:
						switch (opp)
						{
							case itemsopponent.Scissors:
								return itemsme.Rock;
							case itemsopponent.Rock:
								return itemsme.Paper;
							case itemsopponent.Paper:
								return itemsme.Scissors;
						}
						break;
					case winorloose.Loose:
						switch (opp)
						{
							case itemsopponent.Scissors:
								return itemsme.Paper;
							case itemsopponent.Rock:
								return itemsme.Scissors;
							case itemsopponent.Paper:
								return itemsme.Rock;
						}
						break;
					case winorloose.Draw:
						switch (opp)
						{
							case itemsopponent.Scissors:
								return itemsme.Scissors;
							case itemsopponent.Rock:
								return itemsme.Rock;
							case itemsopponent.Paper:
								return itemsme.Paper;
						}
						break;
				}
				throw new Exception("Mhhh...");
			}

		}
	}
}
