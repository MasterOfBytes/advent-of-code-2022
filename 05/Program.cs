﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace _05
{
	class Program
	{
		static void Main(string[] args)
		{
			var fileContent = File.ReadAllLines(@".\input1.txt");

			var stacks = new List<Stack<char>>();
			foreach (var line in fileContent)
			{
				if (line.TrimStart().StartsWith("["))
				{
					var curStack = 0;
					for (var i = 0; i < line.Length; i += 4)
					{
						var asd = line.Substring(i, 3).Replace("[", "").Replace("]", "")[0];

						if (stacks.Count <= curStack)
						{
							stacks.Add(new Stack<char>());
						}
						if (asd != ' ')
						{
							stacks[curStack].Push(asd);
						}

						curStack++;
					}
				}
			}

			for (var i = 0; i < stacks.Count; i++)
			{
				var reversed = new Stack<char>();
				while (stacks[i].Count != 0)
				{
					reversed.Push(stacks[i].Pop());
				}
				stacks[i] = reversed;

				Console.WriteLine($"Stack {i}: {string.Join(',', stacks[i])}");
			}
			Console.WriteLine("-------------------------");

			//M01(fileContent, stacks);   // Auskommentieren, sonst sind die Stacks nicht korrekt!
			Console.WriteLine("-----------  02 --------------");
			M02(fileContent, stacks);
		}

		private static void M01(string[] fileContent, List<Stack<char>> stacks)
		{
			foreach (var line in fileContent)
			{
				if (line.StartsWith("move"))
				{
					var split = line.Split(' ');
					var numberOfMoves = int.Parse(split[1]);
					var stackNumberFrom = int.Parse(split[3]) - 1;
					var stackNumberTo = int.Parse(split[5]) - 1;

					//Console.WriteLine($"Moving {numberOfMoves} from {stackNumberFrom} to {stackNumberTo}");

					for (var i = 0; i < numberOfMoves; i++)
					{
						stacks[stackNumberTo].Push(stacks[stackNumberFrom].Pop());
					}

					for (var i = 0; i < stacks.Count; i++)
					{
						//Console.WriteLine($"Stack {i}: {string.Join(',', stacks[i])}");
					}
				}
			}

			for (var i = 0; i < stacks.Count; i++)
			{
				Console.WriteLine($"Stack {i}: {string.Join(',', stacks[i])}");
			}

			Console.WriteLine($"Result 01: {string.Join("", stacks.Select(s => s.Peek()))}");
		}
		private static void M02(string[] fileContent, List<Stack<char>> stacks)
		{
			foreach (var line in fileContent)
			{
				if (line.StartsWith("move"))
				{
					var split = line.Split(' ');
					var numberOfMoves = int.Parse(split[1]);
					var stackNumberFrom = int.Parse(split[3]) - 1;
					var stackNumberTo = int.Parse(split[5]) - 1;

					//Console.WriteLine($"Moving {numberOfMoves} from {stackNumberFrom} to {stackNumberTo}");

					var movingChars = new List<char>();
					for (var i = 0; i < numberOfMoves; i++)
					{
						movingChars.Add(stacks[stackNumberFrom].Pop());
					}
					movingChars.Reverse();
					for (var i = 0; i < numberOfMoves; i++)
					{
						stacks[stackNumberTo].Push(movingChars[i]);
					}

					for (var i = 0; i < stacks.Count; i++)
					{
						//Console.WriteLine($"Stack {i}: {string.Join(',', stacks[i])}");
					}
				}
			}

			for (var i = 0; i < stacks.Count; i++)
			{
				Console.WriteLine($"Stack {i}: {string.Join(',', stacks[i])}");
			}

			Console.WriteLine($"Result 02: {string.Join("", stacks.Select(s => s.Peek()))}");
		}
	}
}
