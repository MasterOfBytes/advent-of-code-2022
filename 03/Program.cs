﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace _03
{
	class Program
	{
		static void Main(string[] args)
		{
			var fileContent = File.ReadAllLines(@".\input1.txt");

			M01(fileContent);
			M02(fileContent);
		}

		private static void M01(string[] fileContent)
		{
			var sum = 0;
			foreach (var line in fileContent)
			{
				var mid = line.Length / 2;
				var r1 = line.Substring(0, mid);
				var r2 = line.Substring(mid);

				var appearsInBoth = r1.Intersect(r2);

				Console.WriteLine($"{line} => {r1}     {r2}     =>   {string.Join(',', appearsInBoth)}");

				sum += getPrioSum(appearsInBoth);
			}
			Console.WriteLine($"Summe 01: {sum}");
		}

		private static void M02(string[] fileContent)
		{
			var sum = 0;
			for (var i = 0; i < fileContent.Length; i += 3)
			{
				var l1 = fileContent[i];
				var l2 = fileContent[i+1];
				var l3 = fileContent[i+2];

				var appearsInAll = l1.Intersect(l2).Intersect(l3);

				sum += getPrioSum(appearsInAll);
			}
			Console.WriteLine($"Summe 02: {sum}");
		}

		private static int getPrioSum(IEnumerable<char> appearsInBoth)
		{
			var sum = 0;
			foreach (var c in appearsInBoth)
			{
				var val = (int)c - (int)'a' + 1;
				if (c.ToString().ToUpper() == c.ToString()) 
				{
					val = (int)c - (int)'A' + 1;
					val += 26;
				}
				Console.WriteLine($"{c} => {val}");
				sum += val;
			}
			return sum;
		}
	}
}
