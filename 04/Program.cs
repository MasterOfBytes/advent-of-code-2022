﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace _04
{
	class Program
	{
		static void Main(string[] args)
		{
			var fileContent = File.ReadAllLines(@".\input1.txt");

			M01(fileContent);
			M02(fileContent);
		}

		private static void M01(string[] fileContent)
		{
			var sumOfPairs = 0;
			foreach (var line in fileContent)
			{
				var e1string = line.Split(',')[0];
				var e2string = line.Split(',')[1];

				var e1 = getNumbers(e1string);
				var e2 = getNumbers(e2string);

				var all1 = e1.Intersect(e2).Count() == e2.Count();
				var all2 = e2.Intersect(e1).Count() == e1.Count();

				if (all1 || all2)
				{
					sumOfPairs++;
				}

				Console.WriteLine($"{line} =>  {string.Join(',', e1)}    &    {string.Join(',', e2)}      {all1} {all2}");
			}
			Console.WriteLine($"Sum of Pairs 01: {sumOfPairs}");
		}
		private static void M02(string[] fileContent)
		{
			var sumOfPairs = 0;
			foreach (var line in fileContent)
			{
				var e1string = line.Split(',')[0];
				var e2string = line.Split(',')[1];

				var e1 = getNumbers(e1string);
				var e2 = getNumbers(e2string);

				var any1 = e1.Intersect(e2).Count() > 0;
				var any2 = e2.Intersect(e1).Count() > 0;

				if (any1 || any2)
				{
					sumOfPairs++;
				}

				Console.WriteLine($"{line} =>  {string.Join(',', e1)}    &    {string.Join(',', e2)}      {any1} {any2}");
			}
			Console.WriteLine($"Sum of Pairs 02: {sumOfPairs}");
		}

		private static IEnumerable<int> getNumbers(string txt)
		{
			var n1 = int.Parse(txt.Split('-')[0]);
			var n2 = int.Parse(txt.Split('-')[1]);

			for (var i = n1; i <= n2; i++)
			{
				yield return i;
			}
		}
	}
}
